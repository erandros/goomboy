#include <stdio.h>
#include <stdlib.h>

void readRom();

int main(int argc, char** argv) {
    readRom();
}

void readRom() {
    FILE* fp;
    char* content = NULL;
    char* filename = "rom.gb";
    fp = fopen(filename, "r");
    if (fp) {
        fseek(fp, 0, SEEK_END);
        long int size = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        content = malloc(size);
        fread(content, 1, size, fp);
    }
    printf("%.*d\n", 1, content + 0x147);
}